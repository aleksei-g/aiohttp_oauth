import aiohttp_jinja2
from aiohttp import web
from aiohttp_session import get_session


@aiohttp_jinja2.template('login.html')
async def login_oauth(request):
    context = {
        'request': request,
    }
    return context

async def login_oauth_vk(request):
    authorize_url = request.app['vk_client'].get_authorize_url()
    raise web.HTTPFound(authorize_url)


async def login_oauth_vk_colback(request):
    access_token = await request.app['vk_client'].get_access_token(
        request.query.get('code')
    )
    profile = await request.app['vk_client'].get_profile_data(access_token)
    if profile:
        session = await get_session(request)
        user = {
            'id': profile.get('id'),
            'username': (f"{profile.get('first_name', '')} "
                               f"{profile.get('last_name', '')}".strip()),
            'photo_100': profile.get('photo_100'),
        }
        session['user'] = user
        session['access_token'] = access_token
    raise web.HTTPFound(request.app.router['user-friends'].url_for())


async def logout(request):
    session = await get_session(request)
    session.clear()
    raise web.HTTPFound(request.app.router['user-friends'].url_for())
