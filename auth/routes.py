from . import view


def setup_routes(app):
    app.router.add_get('/login', view.login_oauth, name='login')
    app.router.add_get(
        '/login/vk',
        view.login_oauth_vk,
        name='login_oauth_vk',
    )
    app.router.add_get(
        '/login/vk/callback',
        view.login_oauth_vk_colback,
        name='login_oauth_vk_callback',
    )
    app.router.add_get('/logout', view.logout, name='logout')

