from aiohttp import web
from yarl import URL
from urllib.parse import parse_qsl


class OAuth2Client:
    name = 'oauth2'
    authorize_url = None
    access_token_url = None
    method_url = None
    redirect_uri = None
    code_key = 'code'
    access_token_key = 'access_token'
    params = {}

    def __init__(self, client_id, client_secret_key, service_key=None,
                 client_session=None, redirect_uri=None):
        self.client_session = client_session
        self.redirect_uri = redirect_uri
        self.service_key = service_key
        self.client_secret = client_secret_key
        self.params['client_id'] = client_id

    def get_authorize_url(self, authorize_url=None):
        authorize_url = authorize_url or self.authorize_url
        params = {
            'response_type': 'code',
            'redirect_uri': self.redirect_uri,
        }
        params = dict(self.params, **params)
        return URL(authorize_url).with_query(params)

    async def get_access_token(self, code, access_token_url=None):
        access_token_url = access_token_url or self.access_token_url
        if isinstance(code, dict) and self.code_key in code:
            code = code[self.code_key]
        if not code:
            raise web.HTTPBadRequest(reason=f'{self.name}: Bad passed code.')
        params= {
            'client_secret': self.client_secret,
            self.code_key: code,
            'redirect_uri': self.redirect_uri,
        }
        params = dict(self.params, **params)
        async with (self.client_session.get(URL(access_token_url).with_query(
                params))) as resp:
            data = await resp.json()
            if self.access_token_key not in data:
                raise web.HTTPBadRequest(
                    reason=f'{self.name}: error get {self.access_token_key} '
                      f'from {data}')
        return data.get(self.access_token_key)

    async def request(self, method, api_method, access_token=None, params=None):
        params = params or {}
        params = dict(self.params, **params)
        access_token = access_token or self.service_key
        if access_token:
            params[self.access_token_key] = access_token
        url = (URL(self.method_url) / api_method).with_query(params)
        async with self.client_session.request(method, url) as response:
            if response.status / 100 > 2:
                raise web.HTTPBadRequest(
                    reason='HTTP status code: %s' % response.status)
            if 'json' in response.headers.get('CONTENT-TYPE'):
                data = await response.json()
            else:
                data = await response.text()
                data = dict(parse_qsl(data))
            return data


class VKClient(OAuth2Client):
    name = 'vk'
    authorize_url = 'https://api.vk.com/oauth/authorize'
    access_token_url = 'https://oauth.vk.com/access_token'
    method_url = 'https://api.vk.com/method'
    v = '5.95'

    def __init__(self, version=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        params = {
            'v': version or self.v,
            'scope': 'offline',
        }
        self.params.update(params)

    async def get_profile_data(self, access_token, fields=None):
        fields = fields or 'uid,first_name,last_name,photo_100'
        params = {
            'fields': fields,
        }
        profile_raw = await self.request('GET', 'getProfiles', access_token,
                                         params)
        return profile_raw.get('response', [{}])[0]
