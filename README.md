# AioHttp Vk oauth

This AioHttp project implements VK oauth authorization and displays 5 random friends of the user.
Application is available by url [https://aiohttp-vk-oauth.herokuapp.com/](https://aiohttp-vk-oauth.herokuapp.com/).

Project prepared for deployment on [Heroku](https://www.heroku.com/).

## Local installing
1. Create a directory and go to it. Clone project into this folder:

    ```
    $ mkdir project_folder
    $ cd project_folder
    $ git clone https://gitlab.com/aleksei-g/aiohttp_oauth.git .
    ```

2. Create a virtual environment with Python 3.7+ and install all required packeges:

    ```
    $ python3.7 -m venv virtualenv_path
    $ source virtualenv_path/bin/activate
    ```

## Creation VK application
Go to [https://vk.com/editapp?act=create](https://vk.com/editapp?act=create) and register your application for authorization.

Specify the following parameters:
* **Website address**: `https://site.com`
* **Base Domain**: `site.com`
* **Trusted redirect URI** is based on this principle: `http://[domain]/accounts/vk/login/callback/`

After completing all the steps you will receive the `application ID` and `secret key`. You will need this information to configure the oauth provider.

## Before running
Set the value of the environment variables:
* **SITE_URL** (required) - Site URL with scheme, host and port(https://site.com:8080).
* **HOST** (optional) - Application host. Default value 0.0.0.0.
* **PORT** (optional) - Application port. Default value 8080.
* **VK_CLIENT_ID** (required) - VK application ID.
* **VK_CLIENT_SECRET_KEY** (required) - VK application secret key.
* **VK_SERVICE_KEY** (required) - VK application service key.
* **FERNET_KEY** (required) - key for AES encryption/decryption session data into a cookie (must be 32 url-safe base64-encoded bytes). For generate key use next steps:
  ```
  from cryptography import fernet
  fernet.Fernet.generate_key()
  ```

## Run and using
Run the application use the command:
```
(virtualenv_path) $ python3 run.py
```
The application will be available by link [http://localhost:8080/](http://localhost:8080/) (if used default host and port).