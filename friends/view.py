import aiohttp_jinja2
from aiohttp_session import get_session


@aiohttp_jinja2.template('user_friends.html')
async def user_friends(request):
    session = await get_session(request)
    access_token = session.get('access_token')
    context = {
        'request': request,
        'user': session.get('user'),
    }
    if access_token:
        params = {
            'order': 'random',
            'count': '5',
            'fields': 'domain,photo_100',
        }
        friends = await request.app['vk_client'].request(
            method='GET',
            api_method='friends.get',
            params=params,
            access_token=access_token,
        )
        context.update({'friends': friends.get('response', [{}]).get('items')})
    return context