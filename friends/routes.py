from . import view


def setup_routes(app):
    app.router.add_get('/', view.user_friends, name='user-friends')