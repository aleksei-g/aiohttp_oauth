import base64
import os

import aiohttp
import aiohttp_jinja2
import jinja2
from aiohttp import web
from aiohttp_session import setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from cryptography import fernet
from yarl import URL

from auth.oauth_providers.client import VKClient
from auth.routes import setup_routes as auth_setup_routes
from friends.routes import setup_routes as friends_setup_routes
from routes import setup_routes


MAX_AGE_COOKIE = 60 * 60 * 24 * 30


async def create_vk_client(app):
    vk_oauth_client = VKClient(
        client_id=os.getenv('VK_CLIENT_ID'),
        client_secret_key=os.getenv('VK_CLIENT_SECRET_KEY'),
        service_key=os.getenv('VK_SERVICE_KEY'),
        client_session=app['client_session'],
        redirect_uri=str(URL(os.getenv('SITE_URL')).with_path(
            app.router['login_oauth_vk_callback'].canonical))
    )
    app['vk_client'] = vk_oauth_client


async def create_client_session(app):
    app['client_session'] = aiohttp.ClientSession()


async def close_client_session(app):
    await app['client_session'].close()


def create_app():
    app = web.Application()
    fernet_key = os.getenv('FERNET_KEY', None)
    if not bool(fernet_key):
        fernet_key = fernet.Fernet.generate_key()
    session_secret = base64.urlsafe_b64decode(fernet_key)
    setup(app, EncryptedCookieStorage(session_secret, max_age=MAX_AGE_COOKIE))
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader([
        'templates',
        'auth/templates',
        'friends/templates',
    ]))
    setup_routes(app)
    auth_setup_routes(app)
    friends_setup_routes(app)
    app.on_startup.append(create_client_session)
    app.on_startup.append(create_vk_client)
    app.on_cleanup.append(close_client_session)
    return app


app = create_app()


if __name__ == '__main__':
    web.run_app(
        app,
        host = os.getenv('HOST', '0.0.0.0'),
        port = int(os.getenv('PORT', 8080)),
    )
